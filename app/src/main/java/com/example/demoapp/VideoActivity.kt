package com.example.demoapp

import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.demoapp.R
import kotlinx.android.synthetic.main.activity_video.*


class VideoActivity : AppCompatActivity() {

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private var positionBar: SeekBar? = null
    private var elapsedTimeLabel: TextView? = null
    private var reproductionTimeLabel: TextView? = null
    private var mp: MediaPlayer? = null
    private var totalTime = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        elapsedTimeLabel = findViewById(R.id.elapsedTimeLabel);
        reproductionTimeLabel = findViewById(R.id.reproductionTimeLabel);

        // Media Playerの初期化
        mp = MediaPlayer.create(this, R.raw.neco02_720);
        mp?.setLooping(false);
        mp?.seekTo(0);
        totalTime = mp!!.getDuration();

        positionBar = findViewById(R.id.positionBar)
        positionBar!!.setMax(totalTime)
        positionBar!!.setOnSeekBarChangeListener(
            object : OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    if (fromUser) {
                        mp!!.seekTo(progress)
                        positionBar!!.setProgress(progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            }
        );



        playBtn.setOnClickListener {
            if (!mp?.isPlaying()!!){
                mp!!.start();
                playBtn.setBackgroundResource(R.drawable.ic_media_stop_light)
            } else {
                mp!!.pause();
                playBtn.setBackgroundResource(R.drawable.ic_media_play_light)
            }
        }
    }

}